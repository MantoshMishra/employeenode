//load our app server using express somehow..
const express = require('express')
const app = express()
const morgan = require('morgan')
const mysql = require('mysql')
const bodyParser = require('body-parser')

app.use(express.static('./public'))
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json());
app.use(morgan('short'))

const router = require('./routes/employees.js')
app.use(router)

//localhost:3003
app.listen(3003, () =>
{
  console.log("server is listening on 3003");
})
