const express = require('express')
const mysql = require('mysql')
const router = express.Router()
router.get('/employees',(req,res) =>
{
  const connection = getConnection()
  const queryString = "SELECT * FROM employees"
  connection.query(queryString, (err, rows, fields) =>
  {
    if(err)
    {
      res.sendStatus(500);
      return
    }
    res.json(rows)
  })
})

router.get('/employee/:id', (req,res) => 
{
   console.log("Fetching user id " + req.params.id);
   const connection = getConnection()
   const userid= req.params.id
   const queryString = "SELECT * FROM employees WHERE id = ?"

  connection.query(queryString,[userid], (err, rows, fields) => 
  {
    if(err)
    {
      res.sendStatus(500);
      return
    }
    const users = rows.map((row) => 
    {
      return {FirstName: row.first_name, LastName: row.last_name}
    })
    res.json(users)
})
})

router.use(express.static('./public'))
//POST
router.post('/user_create', (req,res) =>
{
  const connection = getConnection()
  const firstName = req.body.create_first_name
  const lastName = req.body.create_last_name
  const queryString = "INSERT INTO employees (first_name, last_name) VALUES (?,?)"
  connection.query(queryString, [firstName, lastName], (err, results, fields) =>
  {
    if(err)
    {
      res.sendStatus(500);
      return
    }
    console.log("Inserted a new employee with id: ", results.insertedId)
    res.end()
  })
  res.end()
})

function getConnection()
{
  return  pool
}

const pool = mysql.createPool({
  connectionLimit: 10,
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'basic_schema'
})

module.exports = router
